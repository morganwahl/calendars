import enum
import math
from decimal import Decimal as D
from typing import Dict, Tuple, Union

from .base import BaseDate, DayStart
from .utils.base import RATA_DIE_EPOCH, Fixed
from .utils.math import remainder

# gregorian-epoch in D&R
EPOCH = Fixed(RATA_DIE_EPOCH + 1)


class Year(int):
    @property
    def is_leap(self) -> bool:
        """
        gregorian-leap-year? in D&R
        """
        return (
            remainder(self, 4) == 0
            and remainder(self, 400) not in (100, 200, 300)
        )

    @property
    def first_day(self) -> 'Date':
        """
        gregorian-new-year in D&R
        """
        return Date(year=self, month=Month.JANUARY, day=1)

    @property
    def last_day(self) -> 'Date':
        """
        gregorian-year-end in D&R
        """
        return Date(year=self, month=Month.DECEMBER, day=31)

    @property
    def range(self) -> Tuple['Date', 'Date']:
        """
        gregorian-year-range in D&R
        """
        return (self.first_day, self.last_day)


# gregorian-year-from-fixed in D&R
def year_from_fixed(date: Fixed) -> Year:
    d0 = date - EPOCH
    n400 = math.floor(d0 / D(146097))
    d1 = d0 % 146097
    n100 = math.floor(d1 / D(36524))
    d2 = d1 % 36524
    n4 = math.floor(d2 / D(1461))
    d3 = d2 % 1461
    n1 = math.floor(d3 / D(365))
    year = sum((
        400 * n400,
        100 * n100,
        4 * n4,
        1 * n1,
    ))
    if n100 == 4 or n1 == 4:
        return Year(year)
    else:
        return Year(year + 1)


# These are individual constants in D&R
class Month(enum.IntEnum):
    JANUARY = 1
    FEBRUARY = 2
    MARCH = 3
    APRIL = 4
    MAY = 5
    JUNE = 6
    JULY = 7
    AUGUST = 8
    SEPTEMBER = 9
    OCTOBER = 10
    NOVEMBER = 11
    DECEMBER = 12


Part = Union[Year, Month, int]


class Date(BaseDate):
    """
    gregorian-date-difference in D&R is just subtraction of these Dates.
    """

    day_start = DayStart.MIDNIGHT
    year: Year
    month: Month
    day: int

    @classmethod
    # type: ignore[override]
    def from_parts(cls, year: Year, month: Month, day: int) -> 'Date':
        """
        fixed-from-gregorian in D&R
        """
        if month <= 2:
            leap_days_correction = 0
        elif year.is_leap:
            leap_days_correction = -1
        else:
            leap_days_correction = -2
        return cls(
            EPOCH - 1
            + (365 * (year - 1))
            + math.floor((year - 1) / D(4))
            - math.floor((year - 1) / D(100))
            + math.floor((year - 1) / D(400))
            + math.floor((1 / D(12)) * (367 * month - 362))
            + leap_days_correction
            + day
        )

    def to_parts(self) -> Dict[str, Part]:  # type: ignore[override]
        """
        gregorian-from-fixed in D&R
        """
        year = year_from_fixed(self.fixed)
        prior_days = (self - year.first_day)

        if self < Date(
            year=year,
            month=Month.MARCH,
            day=1,
        ):
            correction = 0
        elif year.is_leap:
            correction = 1
        else:
            correction = 2
        month = math.floor(
            (1 / D(367))
            * (
                12 * (prior_days + correction)
                + D(373)
            )
        )
        day = 1 + (self - Date(
            year=year, month=Month(month), day=1
        ))
        return {'year': year, 'month': Month(month), 'day': day}

    def day_number(self) -> int:
        """
        day-number in D&R
        """
        return self - self.year.last_day - 1

    def days_remaining(self) -> int:
        """
        days-remaining in D&R
        """
        return self.year.last_day - self
