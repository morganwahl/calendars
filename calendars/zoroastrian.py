from enum import IntEnum
from typing import Dict, Union

from . import egyptian
from .base import BaseDate
from .utils.base import Fixed

EPOCH = Fixed(230638)


class Day(IntEnum):
    HORMUZ = 1
    BAHMAN = 2
    ORDIBEHESHT = 3
    SHAHRIVAR = 4
    ESFANDARMUD = 5
    XORDAD = 6
    MORDAD = 7
    DIY_BE_AZAR = 8
    AZAR = 9
    ABAN = 10
    XOR = 11
    MAH = 12
    TIR = 13
    GOOSH = 14
    DIY_BE_MEHR = 15
    MEHR = 16
    SORUSH = 17
    RASHN = 18
    FARVARDIN = 19
    BAHRAM = 20
    RAM = 21
    BAD = 22
    DIY_BE_DIN = 23
    DIN = 24
    ARD = 25
    ASHTAD = 26
    ASMAN = 27
    ZAMYAD = 28
    MARESFAND = 29
    ANIRAN = 30


class EpagomenaeDay(IntEnum):
    AHNAD = 1
    ASHNAD = 2
    ESFANDARMUD = 3
    AXSHATAR = 4
    BEHESHT = 5


class Date(BaseDate):
    @classmethod
    def from_parts(   # type: ignore[override]
        cls,
        year: int,
        month: int,
        day: Union[int, Day, EpagomenaeDay]
    ) -> 'Date':
        return cls(
            EPOCH
            + int(egyptian.Date(year=year, month=month, day=day))
            - egyptian.EPOCH
        )

    def to_parts(self) -> Dict[str, Union[int, Day]]:  # type: ignore[override]  # noqa: E501
        parts = egyptian.Date(self + egyptian.EPOCH - EPOCH).to_parts()
        parts['month'] = int(parts['month'])
        if parts['month'] != 13:
            parts['day'] = Day(parts['day'])
        return parts
