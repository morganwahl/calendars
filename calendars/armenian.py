from enum import IntEnum

from . import egyptian
from .base import BaseDate
from .utils.base import Duration, Fixed

# armenian-epoch in D&R
EPOCH = Fixed(201443)


class Month(IntEnum):
    NAWASARDI = 1
    HORI = 2
    SAHMI = 3
    TRE = 4
    KALOCH = 5
    ARACH = 6
    MEHEKANI = 7
    AREG = 8
    AHEKANI = 9
    MARERI = 10
    MARGACH = 11
    HROTICH = 12
    AWELEACH = 13

    @property
    def named(self) -> bool:
        return self != self.AWELEACH

    def length(self) -> Duration:
        return Duration(days=30) if self.named else Duration(days=5)


class Date(BaseDate):
    @classmethod
    def from_parts(cls, year, month, day) -> 'Date':
        """
        fixed-from-armenian in D&R
        """
        return cls(
            egyptian.Date(year=year, month=month, day=day)
            + Duration(days=EPOCH)
            - Duration(days=egyptian.EPOCH)
        )

    def to_parts(self):
        """
        armenian-from-fixed in D&R
        """
        parts = egyptian.Date(
            self
            + Duration(days=egyptian.EPOCH)
            - Duration(days=EPOCH)
        ).to_parts()
        parts['month'] = Month(parts['month'])
        return parts
