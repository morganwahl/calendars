import enum
import math
from decimal import Decimal as D

from . import clock
from .base import BaseDate, DayStart
from .gregorian import year_from_fixed as gregorian_year_from_fixed
from .utils.base import Moment
from .utils.earth import Location
from .utils.math import adjusted_remainder, remainder
from .utils.search import day_search
from .utils.sun import (
    estimate_prior_solar_longitude,
    solar_longitude,
    solar_longitude_after,
    winter
)
from .utils.time import standard_from_universal, universal_from_standard
from .utils.units import Angle, Length

LOCATION_BEFORE_1929 = Location(
    latitude=Angle(degrees=39, minutes=55),
    longitude=Angle(degrees=116, minutes=25),
    altitude=Length(D('43.5')),
    time_offset=clock.TimeDelta(hours=D(1397)/D(180)),
)
LOCATION_AFTER_1929 = Location(
    latitude=Angle(degrees=39, minutes=55),
    longitude=Angle(degrees=116, minutes=25),
    altitude=Length(D('43.5')),
    time_offset=clock.TimeDelta(hours=8)
)


def location(moment: Moment) -> Location:
    """
    chinese-location in D&R
    """
    year = gregorian_year_from_fixed(moment.to_fixed())
    if year < 1929:
        return LOCATION_BEFORE_1929
    else:
        return LOCATION_AFTER_1929


class SolarTerm(Angle, enum.Enum):
    """
    major terms: 中气 zhōngqì
    minor terms: 节气 jiéqì
    """
    # pinyin
    LICHUN = Angle(degrees=315)
    YUSHUI = Angle(degrees=330)
    JINGZHE = Angle(degrees=345)
    CHUNFEN = Angle(degrees=0)
    QINGMING = Angle(degrees=15)
    GUYU = Angle(degrees=30)
    LIXIA = Angle(degrees=45)
    XIAOMAN = Angle(degrees=60)
    MANGZHONG = Angle(degrees=75)
    XIAZHI = Angle(degrees=90)
    XIAOSHU = Angle(degrees=105)
    DASHU = Angle(degrees=120)
    LIQIU = Angle(degrees=135)
    CHUSHU = Angle(degrees=150)
    BAILU = Angle(degrees=165)
    QIUFEN = Angle(degrees=180)
    HANLU = Angle(degrees=195)
    SHUANGJIANG = Angle(degrees=210)
    LIDONG = Angle(degrees=225)
    XIAOXUE = Angle(degrees=240)
    DAXUE = Angle(degrees=255)
    DONGZHI = Angle(degrees=270)
    XIAOHAN = Angle(degrees=285)
    DAHAN = Angle(degrees=300)


class MajorTerm(Angle, enum.Enum):
    YUSHUI = Angle(degrees=330)
    CHUNFEN = Angle(degrees=0)
    GUYU = Angle(degrees=30)
    XIAOMAN = Angle(degrees=60)
    XIAZHI = Angle(degrees=90)
    DASHU = Angle(degrees=120)
    CHUSHU = Angle(degrees=150)
    QIUFEN = Angle(degrees=180)
    SHUANGJIANG = Angle(degrees=210)
    XIAOXUE = Angle(degrees=240)
    DONGZHI = Angle(degrees=270)
    DAHAN = Angle(degrees=300)


def solar_longitude_on_or_after(lam: Angle, date: Moment) -> Moment:
    """
    chinese-solar-longitude-on-or-after in D&R
    """
    t = solar_longitude_after(
        lam, universal_from_standard(date, location(date))
    )
    return standard_from_universal(t, location(t))


def midnight_in_china(date: Moment) -> Moment:
    """
    midnight-in-china in D&R
    """
    return universal_from_standard(date, location(date))


def major_solar_term_on_or_after(date: Moment) -> MajorTerm:
    """
    major-solar-term-on-or-after in D&R
    """
    s = solar_longitude(midnight_in_china(date))
    el = remainder((30 * math.ceil(s / 30)), 360)
    return MajorTerm[solar_longitude_on_or_after(el, date)]


def current_minor_solar_term(date: Moment) -> SolarTerm:
    """
    current-minor-solar-term in D&R
    """
    s = solar_longitude(universal_from_standard(date, location(date)))
    return SolarTerm[adjusted_remainder(
        3 + math.floor(
            (s - Angle(degrees=15)) / Angle(degrees=30)
        ),
        12,
    )]


def minor_solar_term_on_or_after(date):
    """
    minor-solar-term-on-or-after in D&R
    """
    s = solar_longitude(midnight_in_china(date))
    el = remainder(
        30 * math.ceil(
            (s - Angle(degrees=15)) / 30
        ) + Angle(degrees=15),
        360,
    )
    return solar_longitude_on_or_after(el, date)


def winter_solstice_on_or_before(date):
    """
    chinese-winter-soltice-on-or-before in D&R.
    """
    def test(day):
        return winter < solar_longitude(midnight_in_china(day + 1))
    approx = estimate_prior_solar_longitude(
        winter,
        midnight_in_china(date + 1),
    )
    day_search(
        math.floor(approx) - 1,
        test,
    )


class Date(BaseDate):
    day_start = DayStart.MIDNIGHT

    @classmethod
    def from_parts(cls, cycle, year, month, leap, day):
        raise NotImplementedError

    def to_parts(self):
        raise NotImplementedError

    def current_major_solar_term(self) -> SolarTerm:
        """
        current-major-solar-term in D&R
        """
        sun_longitude = solar_longitude(
            universal_from_standard(self, location(self)))
        return SolarTerm(adjusted_remainder(
            Angle(degrees=2) + math.floor(sun_longitude / Angle(degrees=30)),
            Angle(degrees=12),
        ))


def main():
    """
    Print out some calendar events soon.
    """


if __name__ == '__main__':
    main()
