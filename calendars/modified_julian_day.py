from typing import Dict

from .base import BaseDate, DayStart
from .utils.base import RATA_DIE_EPOCH

# mjd-epoch in D&R
EPOCH = 678576 + RATA_DIE_EPOCH


class Date(BaseDate):

    day_start = DayStart.MIDNIGHT

    @classmethod
    # type: ignore[override]
    def from_parts(cls, modified_julian_day: int) -> 'Date':
        """
        mjd-from-fixed in D&R
        """
        return cls(modified_julian_day - EPOCH)

    def to_parts(self) -> Dict['str', int]:   # type: ignore[override]
        """
        fixed-from-mjd in D&R
        """
        return {'modified_julian_day': self + EPOCH}
