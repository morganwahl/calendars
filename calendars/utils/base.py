import math
from decimal import Decimal
from typing import Union

from .math import DI, remainder

RATA_DIE_EPOCH = 0


class Duration(Decimal):
    """
    An length of time, in days.
    """

    def __new__(
        cls,
        days: DI=0,
    ) -> 'Duration':
        return Decimal.__new__(cls, sum((
            Decimal(days),
        )))

    def __repr__(self) -> str:
        return "Duration(days={!r})".format(Decimal(self))

    @property
    def fractional(self) -> bool:
        return self != int(self)


class Fixed(int):
    """
    An integral count of days since :const:`RATA_DIE_EPOCH`.

    To see the date of that epoch, just do Date(RATA_DIE_EPOCH) in your
    calendar of choice.
    """
    # TODO document inteaction with different day starts (e.g. sunrise, sunset,
    # midnight, noon).

    def __add__(self, other: Union[Duration, int]) -> 'Fixed':
        if isinstance(other, (Fixed, Moment, Offset)):
            raise TypeError(
                "Adding points in time together doesn't make sense.")
        if isinstance(other, Duration):
            if other.fractional:
                raise ValueError(
                    "Can't add a non-integer number of days to a Fixed.")
        result = int(self) + other
        return Fixed(result)

    def __sub__(self, other: Union[Duration, int]) -> 'Fixed':
        return Fixed(self + - other)

    def __repr__(self) -> str:
        return "Fixed({!r})".format(int(self))


class Offset(Decimal):
    """
    An offset within a day, as a number between 0 and 1.
    """
    # TODO actually validate in __new__ that it's between 0 and 1
    def __add__(self, other: Union[Duration, Decimal, DI]) -> 'Moment':  # type: ignore[override] # noqa:E501
        if isinstance(other, (Fixed, Moment, Offset)):
            raise TypeError(
                "Adding points in time together doesn't make sense.")
        result = Decimal(self) + Decimal(other)
        return Moment(remainder(Offset(result), 1))

    def __repr__(self) -> str:
        return "Offset({!r})".format(Decimal(self))


class Moment(Decimal):
    """
    A particular moment in time, as a possibly non-integral number of days
    since :const:`RATA_DIE_EPOCH`.
    """

    def __add__(self, other: Union[Duration, DI]) -> 'Moment':  # type: ignore[override]  # noqa: E501
        if isinstance(other, (Fixed, Moment, Offset)):
            raise TypeError(
                "Adding points in time together doesn't make sense.")
        result = Decimal(self) + Decimal(other)
        return Moment(result)

    def __sub__(self, other: Union[Duration, DI]) -> 'Moment':  # type: ignore[override]  # noqa: E501
        return Moment(self + - Decimal(other))

    @classmethod
    def from_fixed_and_offset(cls, fixed: Fixed, offset: Offset) -> 'Moment':
        return Moment(fixed) + Duration(offset)

    def to_fixed(self) -> Fixed:
        """
        fixed-from-moment in D&R
        """
        return Fixed(math.floor(self))

    def to_offset(self) -> Offset:
        """
        time-from-moment in D&R
        """
        return Offset(remainder(self, 1))
