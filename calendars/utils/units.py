import math
from decimal import Decimal as D

from .math import DI, cos, sin


class Length(D):
    """
    Length in meters.
    """

    def __new__(
        cls,
        meters: DI=0,
    ) -> 'Length':
        return D.__new__(cls, sum((
            D(meters),
        )))

    def __repr__(self) -> str:
        return 'Length(meters={})'.format(super().__repr__())


class Angle(D):

    # How many in one complete turn?
    TOTAL_DEGREES = 360
    TOTAL_MINUTES = 60 * TOTAL_DEGREES
    TOTAL_SECONDS = 60 * TOTAL_MINUTES

    def __new__(
        cls,
        turns: DI=0,
        degrees: DI=0,
        minutes: DI=0,
        seconds: DI=0,
    ) -> 'Angle':
        return D.__new__(cls, sum((
            D(turns),
            D(degrees) / cls.TOTAL_DEGREES,
            D(minutes) / cls.TOTAL_MINUTES,
            D(seconds) / cls.TOTAL_SECONDS,
        )))

    def __repr__(self) -> str:
        return 'Angle(turns={})'.format(super().__repr__())

    @property
    def radians(self) -> D:
        return self * D(math.tau)

    @property
    def sine(self) -> D:
        return D(sin(self.radians))

    @property
    def cosine(self) -> D:
        return D(cos(self.radians))
