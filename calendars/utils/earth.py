from .base import Duration
from .units import Angle, Length


class Location:
    def __init__(
        self,
        latitude: Angle,
        longitude: Angle,
        altitude: Length,
        time_offset: Duration,
    ):
        self.latitude = latitude
        self.longitude = longitude
        self.altitude = altitude
        self.time_offset = time_offset
