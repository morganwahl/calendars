from decimal import Decimal
from typing import Callable

from .base import Duration, Fixed, Moment
from .units import Angle


class SearchingTooLong(RuntimeError):
    pass


def day_search(d0: Fixed, phi: Callable[[Fixed], bool]) -> Fixed:
    """
    MIN operator without stuff above it in D&R, equation (1.28).
    """
    max_search = Duration(days=366)
    for d in (range(d0, d0 + max_search)):
        d = Fixed(d)
        if phi(d):
            return d
    raise SearchingTooLong((
        "Couldn't find a {} within {} days after {}"
    ).format(phi, max_search, d0))


def bisection_search(
    mu: Decimal,
    nu: Decimal,
    phi: Callable[[Decimal, Decimal], bool],
    psi: Callable[[Decimal], bool],
) -> Decimal:
    """
    MIN operator with suff above it in D&R, equation (1.31)

    phi(l, u)
    MIN { psi(zeta) }
    zeta in [mu, nu]

    appropriate for inverting increasing functions.

    phi is a function that takes two values and returns true if they're close
    enough to end the search.

    psi is a function that returns true when a value is greater than the one
    we're looking for.
    """
    x = (Decimal(nu) + Decimal(mu)) / 2
    if phi(mu, nu):
        return x
    elif psi(x):
        return bisection_search(mu, x, phi, psi)
    else:
        return bisection_search(x, nu, phi, psi)


def angle_search(
    func: Callable[[Moment], Angle],
    y: Angle,
    a: Moment,
    b: Moment,
) -> Moment:
    """
    Equation (1.32) in D&R.

    f^-1(y, [a,b])

    u-l<10^-5
    MIN (f(x)-y) mod 360 < 180o
    x in [a,b]
    """
    def small_enough(u: Moment, l: Moment) -> bool:
        return u - l < 10 ** -5

    def too_big(x: Moment) -> bool:
        return (func(x) - y) % 360 < 180

    return Moment(bisection_search(a, b, small_enough, too_big))
