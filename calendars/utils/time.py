from decimal import Decimal as D

from .. import gregorian
from .base import Duration, Moment
from .earth import Location
from .units import Angle


def zone_from_longitude(phi: Angle) -> Duration:
    """
    zone-from-longitude in D&R.
    """
    return Duration(days=phi / Angle(degrees=360))


def universal_from_local(tl: Moment, locale: Location) -> Moment:
    """
    universal-from-local in D&R.
    """
    return Moment(tl - zone_from_longitude(locale.longitude))


def local_from_universal(tu: Moment, locale: Location) -> Moment:
    """
    local-from-universal in D&R.
    """
    return tu + zone_from_longitude(locale.longitude)


def standard_from_universal(tu: Moment, locale: Location) -> Moment:
    """
    standard-from-universal in D&R.
    """
    return tu + locale.time_offset


def universal_from_standard(ts: Moment, locale: Location) -> Moment:
    """
    universal-from-standard in D&R.
    """
    return ts - locale.time_offset


def ephemeris_correction(time: Moment) -> Duration:
    """
    ephemeris-correction in D&R
    """
    year = gregorian.year_from_fixed(time.to_fixed())
    c = (1 / D(36525)) * (
        gregorian.Date(
            year=year,
            month=gregorian.Month.JULY,
            day=gregorian.DayOfMonth(1),
        )
        - gregorian.Date(
            year=gregorian.Year(1900),
            month=gregorian.Month.JANUARY,
            day=gregorian.DayOfMonth(1),
        )
    )
    x = Duration(D('0.5')) + (
        gregorian.Date(
            year=year,
            month=gregorian.Month.JANUARY,
            day=gregorian.DayOfMonth(1),
        )
        - gregorian.Date(
            year=gregorian.Year(1810),
            month=gregorian.Month.JANUARY,
            day=gregorian.DayOfMonth(1),
        )
    )
    if year >= 1988 and year <= 2019:
        return Duration((1 / D(86400)) * (year - 1933))
    if year >= 1900 and year <= 1987:
        return Duration(D(sum((
            # Decimals raise an error when taking to the zeroth power.
            (1) * D('-0.00002'),
            (c ** 1) * D('+0.000297'),
            (c ** 2) * D('+0.025184'),
            (c ** 3) * D('-0.181133'),
            (c ** 4) * D('+0.553040'),
            (c ** 5) * D('-0.861938'),
            (c ** 6) * D('+0.677066'),
            (c ** 7) * D('-0.212591'),
        ))))
    if year >= 1800 and year <= 1899:
        return Duration(D(sum((
            (1) * D('-0.000009'),
            (c ** 1) * D('+0.003844'),
            (c ** 2) * D('+0.083563'),
            (c ** 3) * D('+0.865736'),
            (c ** 4) * D('+4.867575'),
            (c ** 5) * D('+15.845535'),
            (c ** 6) * D('+31.332267'),
            (c ** 7) * D('+38.291999'),
            (c ** 8) * D('+28.316289'),
            (c ** 9) * D('+11.636204'),
            (c ** 10) * D('+2.043794'),
        ))))
    if year >= 1700 and year <= 1799:
        offset = (year - 1700)
        return Duration((1 / D(86400)) * sum((
            (1) * D('+8.118780842'),
            (offset ** 1) * D('-0.005092142'),
            (offset ** 2) * D('+0.003336121'),
            (offset ** 3) * D('-0.0000266484'),
        )))
    if year >= 1620 and year <= 1699:
        offset = (year - 1600)
        return Duration((1 / D(86400)) * sum((
            (1) * D('+196.58333'),
            (offset ** 1) * D('-4.0675'),
            (offset ** 2) * D('+0.0219167'),
        )))
    return Duration((1 / D(86400)) * (((x ** 2) / D(41048480)) - 15))


def dynamical_from_universal(time: Moment) -> Moment:
    return Moment(time + ephemeris_correction(time))
