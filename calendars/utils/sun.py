from decimal import Decimal as D
from enum import Enum

from .. import gregorian
from .base import Duration, Moment, Offset
from .math import remainder, sin
from .search import angle_search
from .time import dynamical_from_universal
from .units import Angle

J2000 = (
    Moment.from_fixed_and_offset(
        gregorian.new_year(gregorian.Year(2000)),
        Offset(D('.5')),
    )
)


# mean-tropical-year in D&R
MEAN_TROPICAL_YEAR = Duration(days=D('365.242189'))


def julian_centuries(time: Moment) -> D:
    """
    julian-centuries in D&R.
    """
    return 1 / D(36525) * (dynamical_from_universal(time) - J2000)


def aberration(time: Moment) -> Angle:
    """
    aberration in D&R.
    """
    c = julian_centuries(time)
    angle = Angle(degrees=D('0.0000974'))
    angle = Angle(angle * Angle(
        Angle(degrees=D('177.63'))
        + c * Angle('35999.01848')
    ).cosine)
    angle = Angle(angle - Angle(degrees=D('-0.005575')))
    return angle


def nutation(time: Moment) -> Angle:
    """
    nutation in D&R.
    """
    c = julian_centuries(time)
    A = sum((
        (1) * Angle(degrees=D('124.90')),
        (c ** 1) * Angle(degrees=-D('1934.134')),
        (c ** 2) * Angle(degrees=D('0.002063')),
    ))
    B = sum((
        (1) * Angle(degrees=D('201.11')),
        (c ** 1) * Angle(degrees=D('72001.5377')),
        (c ** 2) * Angle(degrees=D('0.00057')),
    ))
    return Angle(sum((
        Angle(A).sine * Angle(degrees=-D('0.004778')),
        Angle(B).sine * Angle(degrees=-D('0.0003667')),
    )))


def solar_longitude(time: Moment) -> Angle:
    """
    solar-longitude in D&R.
    """
    c = julian_centuries(time)

    # Table 13.1 in D&R.
    data_input = """
    403406 270.54861     0.9287892
    195207 340.19128 35999.1376958
    119433  63.91854 35999.4089666
    112392 331.26220 35998.7287385
      3891 317.843   71998.20261
      2819  86.631   71998.4403
      1721 240.052   36000.35726
       660 310.26    71997.4812
       350 247.23    32964.4678
       334 260.87      -19.4410
       314 297.82   445267.1117
       268 343.14    45036.8840
       242 166.79        3.1008
       234  81.53    22518.4434
       158   3.50      -19.9739
       132 132.75    65928.9345
       129 182.95     9038.0293
       114 162.03     3034.7684
        99  29.8     33718.148
        93 266.4      3034.448
        86 249.2     -2280.773
        78 157.6     29929.992
        72 257.8     31556.493
        68 185.1       149.588
        64  69.9      9037.750
        46   8      107997.405
        38 197.1     -4444.176
        37 250.4       151.771
        32  65.3     67555.316
        29 162.7     31556.080
        28 341.5     -4561.540
        27 291.6    107996.706
        27  98.5      1221.655
        25 146.7     62894.167
        24 110       31437.369
        21   5.2     14578.298
        21 342.6    -31931.757
        20 230.9     34777.243
        18 256.1      1221.999
        17  45.3     62894.511
        14 242.9     -4442.039
        13 115.2    107997.909
        13 151.8       119.066
        13 285.3     16859.071
        12  53.3        -4.578
        10 126.6     26895.292
        10 205.7       -39.127
        10  85.9     12297.536
        10 146.1     90073.778
    """
    data = [
        [D(number) for number in row.strip().split()]
        for row in data_input.splitlines() if row.strip()
    ]

    lam = (
        Angle(degrees=D('282.7771834'))
        + (Angle(degrees=D('36000.76953744')) * c)
        + (Angle(degrees=D('0.000005729577951308232')) * sum((
            x * sin(y + z * c)
            for x, y, z in data
        )))
    )
    return Angle(
        remainder((lam + aberration(time) + nutation(time)), Angle(turns=1)))


def solar_longitude_after(lam: Angle, t: Moment) -> Moment:
    """
    solar-longitude-after in D&R
    """
    rate = MEAN_TROPICAL_YEAR / Angle(turns=1)
    tau = t + (
        rate * (
            remainder((lam - solar_longitude(t)), Angle(turns=1))
        )
    )
    a = Moment(max(t, tau - Duration(days=5)))
    b = tau + Duration(days=5)
    return angle_search(solar_longitude, lam, a, b)


def estimate_prior_solar_longitude(lam: Angle, t: Moment) -> Moment:
    """
    estimate-prior-solar-longitude in D&R.
    """
    rate = MEAN_TROPICAL_YEAR / Angle(turns=1)
    tau = Moment(t - (rate * (
        remainder(
            solar_longitude(t) - lam,
            Angle(turns=1),
        )
    )))
    delta = remainder(
        solar_longitude(tau) - lam + Angle(turns=D('.5')),
        Angle(turns=1),
    ) - Angle(turns=D('.5'))
    return Moment(min(t, tau - (rate * delta)))


class Season(Angle, Enum):
    WINTER = Angle(degrees=270)
    SPRING = Angle(degrees=0)
    SUMMER = Angle(degrees=90)
    FALL = Angle(degrees=180)
