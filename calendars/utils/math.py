import math
from decimal import Decimal, getcontext
from typing import Callable, Union

DI = Union[Decimal, str, int, float]
"""
The type of args to the Decimal constructor.
"""


def remainder(number: DI, divisor: DI) -> Decimal:
    """
    This is the same as the `%` for floats and ints in Python, but different
    than how that operator works for Decimal.

    mod operator defined in D&R.
    """
    number = Decimal(number)
    divisor = Decimal(divisor)
    return number - (divisor * math.floor(number / divisor))


mod = remainder


def adjusted_remainder(number: DI, divisor: DI) -> Decimal:
    """
    amod in D&R.
    """
    number = Decimal(number)
    divisor = Decimal(divisor)
    # The % operator behaves differently for Decimal than it does for int and
    # float, so use our mod function instead.
    remainder = mod(number, divisor)
    if remainder == 0:
        return divisor
    else:
        return remainder


amod = adjusted_remainder


def greatest_common_divisor(x: Decimal, y: Decimal) -> Decimal:
    """
    gcd in D&R
    """
    if y == 0:
        return x
    return greatest_common_divisor(y, remainder(x, y))


gcd = greatest_common_divisor


def least_common_multiple(x: Decimal, y: Decimal) -> Decimal:
    """
    lcm in D&R
    """
    return (x * y) / greatest_common_divisor(x, y)


lcm = least_common_multiple


def sum_range_while(
    f: Callable[[Decimal], Decimal],
    k: Decimal,
    p: Callable[[Decimal], bool],
) -> Decimal:
    """
    Summation operator defined as (1.27) in D&R
    """
    result = Decimal(0)
    i = k
    while p(i):
        result += f(i)
        i += 1
    return result


def pi() -> Decimal:
    """Compute Pi to the current precision.

    >>> print(pi())
    3.141592653589793238462643383

    Taken directly from :mod:`decimal` docs.
    """
    getcontext().prec += 2  # extra digits for intermediate steps
    lasts = Decimal(0)
    t = Decimal(3)
    s = Decimal(3)
    n = 1
    na = 0
    d = 0
    da = 24
    while s != lasts:
        lasts = s
        n, na = n+na, na+8
        d, da = d+da, da+32
        t = (t * n) / d
        s += t
    getcontext().prec -= 2
    return +s               # unary plus applies the new precision


def sin(x: Decimal) -> Decimal:
    """Return the sine of x as measured in radians.

    The Taylor series approximation works best for a small value of x.
    For larger values, first compute x = x % (2 * pi).

    >>> print(sin(Decimal('0.5')))
    0.4794255386042030002732879352
    >>> print(sin(0.5))
    0.479425538604
    >>> print(sin(0.5+0j))
    (0.479425538604+0j)

    Taken directly from the :mod:`decimal` docs.
    """
    getcontext().prec += 2
    i = 1
    lasts = Decimal(0)
    s = x
    fact = 1
    num = x
    sign = 1
    while s != lasts:
        lasts = s
        i += 2
        fact *= i * (i-1)
        num *= x * x
        sign *= -1
        s += num / fact * sign
    getcontext().prec -= 2
    return +s


def cos(x):
    """Return the cosine of x as measured in radians.

    The Taylor series approximation works best for a small value of x.
    For larger values, first compute x = x % (2 * pi).

    >>> print(cos(Decimal('0.5')))
    0.8775825618903727161162815826
    >>> print(cos(0.5))
    0.87758256189
    >>> print(cos(0.5+0j))
    (0.87758256189+0j)

    Taken directly from :mod:`decimal` docs.
    """
    getcontext().prec += 2
    i, lasts, s, fact, num, sign = 0, 0, 1, 1, 1, 1
    while s != lasts:
        lasts = s
        i += 2
        fact *= i * (i-1)
        num *= x * x
        sign *= -1
        s += num / fact * sign
    getcontext().prec -= 2
    return +s
