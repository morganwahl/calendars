from decimal import Decimal
from enum import Enum
from typing import Dict, Generic, Optional, Type, TypeVar, Union

from .utils.base import Duration, Fixed, Moment, Offset

T = TypeVar('T')
U = TypeVar('U')


class PartsMixin(Generic[T, U]):
    def __new__(  # type: ignore[misc]
        cls,
        other: Optional[Union[T, U]]=None,
        **kwargs: object
    ) -> 'T':
        if other is None:
            new_obj = cls.from_parts(**kwargs)
        else:
            # Directly above this class in the MRO is 'Generic'. We want the
            # __new__ method on the class after that one.
            for i, c in enumerate(cls.__mro__):
                if c is PartsMixin:
                    superclass = cls.__mro__[i + 2]
                    break
            new_obj = superclass.__new__(cls, other)
        return new_obj

    @classmethod
    def from_parts(self, **kwargs: object) -> T:
        raise NotImplementedError

    def to_parts(self) -> Dict[str, object]:
        raise NotImplementedError

    _parts: Optional[Dict[str, object]] = None

    def __getattr__(self, name: str) -> object:
        if self._parts is None:
            self._parts = self.to_parts()
        if name in self._parts:
            return self._parts[name]
        return getattr(super(), name)  # type: ignore[misc]

    def __repr__(self) -> str:
        return "{}(**{!r})".format(self.__class__.__name__, self.to_parts())


class BaseTimeDelta(
    PartsMixin['BaseTimeDelta', Union[Duration, Decimal]],
    Duration
):
    pass


class DayStart(Enum):
    # These fractional values are just symbolic; actual times of these events
    # vary.
    MIDNIGHT = 0
    DAWN = .2
    SUNRISE = .25
    NOON = .5
    SUNSET = .75


class BaseDate(PartsMixin['BaseDate', Union[Fixed, int]], Fixed):
    """
    The day represented by a Fixed, in a particular calendar.

    Conversions are performed at noon.
    """

    day_start: DayStart = NotImplemented

    @property
    def fixed(self) -> Fixed:
        return Fixed(self)


class BaseTime(PartsMixin['BaseTime', Union[Offset, Decimal]], Offset):
    """
    A single point within a day.
    """
    pass


class BaseDateTime(Moment):

    date_class: Type[BaseDate] = NotImplemented
    time_class: Type[BaseTime] = NotImplemented

    def __new__(
        cls,
        date: Optional[BaseDate]=None,
        time: Optional[BaseTime]=None,
        moment: Optional[Union[Moment, Decimal]]=None,
    ) -> 'BaseDateTime':
        if moment is None and date is None:
            raise ValueError("Must specify give a moment or date.")

        if moment is not None:
            moment = Moment(moment)
            fixed = moment.to_fixed()
            offset = moment.to_offset()
            date = cls.date_class(fixed)
            time = cls.time_class(offset)
        else:
            date = cls.date_class(date)
            time = cls.time_class(time)
            moment = Moment.from_fixed_and_offset(date, time)

        new_obj = super(cls).__new__(moment)
        new_obj.date = date
        new_obj.time = time

        return new_obj
