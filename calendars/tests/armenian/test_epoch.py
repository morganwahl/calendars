from unittest import TestCase

from calendars import julian
from calendars.armenian import EPOCH


class TestEpoch(TestCase):
    def test_julian(self) -> None:
        self.assertEqual(julian.Date(EPOCH), julian.Date(
            year=julian.Year(552),
            month=julian.Month.JULY,
            day=11,
        ))
