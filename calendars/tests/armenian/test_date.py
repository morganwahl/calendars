from unittest import TestCase

from calendars.armenian import Date

from ..test_date import TestDateMethods


class TestDate(TestDateMethods, TestCase):
    date_class = Date
    sample_data = (
        (-214193, {'year': -1138, 'month': 4, 'day': 10}),
        (-61387, {'year': -720, 'month': 12, 'day': 6}),
        (25469, {'year': -482, 'month': 11, 'day': 22}),
        (764652, {'year': 1544, 'month': 1, 'day': 15})
    )
