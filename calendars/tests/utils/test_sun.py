from decimal import Decimal as D
from unittest import TestCase

from calendars import clock, gregorian
from calendars.utils.base import Moment
from calendars.utils.earth import Location
from calendars.utils.sun import solar_longitude_after
from calendars.utils.time import standard_from_universal
from calendars.utils.units import Angle, Length


class TestSolarLongitudeAfter(TestCase):
    def test_urbana_winter(self) -> None:
        # From D&R page 191.

        urbana = Location(
            latitude=Angle(degrees=D('40.1')),
            longitude=Angle(degrees=D('-88.2')),
            altitude=Length(meters=225),
            time_offset=clock.TimeDelta(hours=-6),
        )

        def urbana_winter(gregorian_year: gregorian.Year) -> Moment:
            return standard_from_universal(
                solar_longitude_after(
                    Angle(degrees=270),
                    Moment(gregorian.Date(
                        year=gregorian_year,
                        month=gregorian.Month.JANUARY,
                        day=gregorian.DayOfMonth(1),
                    )),
                ),
                urbana,
            )

        self.assertEqual(
            Moment('730475.31751'),
            urbana_winter(gregorian.Year(2000))
        )
