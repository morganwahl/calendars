from unittest import TestCase

from calendars import gregorian

from ..test_date import TestDateMethods


class TestGregorianDate(TestDateMethods, TestCase):
    date_class = gregorian.Date
    sample_data = (
        (-214193, {'year': -586, 'month': 7, 'day': 24}),
        (-61387, {'year': -168, 'month': 12, 'day': 5}),
        (25469, {'year': 70, 'month': 9, 'day': 24}),
        (764652, {'year': 2094, 'month': 7, 'day': 18})
    )
