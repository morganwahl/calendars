from unittest import TestCase

from calendars import julian
from calendars.egyptian import EPOCH
from calendars.utils.base import Fixed


class TestEpoch(TestCase):
    def test_rata_die(self) -> None:
        self.assertEqual(Fixed(EPOCH), Fixed(-272787))

    def test_julian(self) -> None:
        self.assertEqual(EPOCH, julian.Date(
            year=julian.Year(747, bce=True),
            month=julian.Month.FEBRUARY,
            day=julian.DayOfMonth(26),
        ).fixed)
