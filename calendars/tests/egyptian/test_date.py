from unittest import TestCase

from calendars.egyptian import Date

from ..test_date import TestDateMethods


class TestDate(TestDateMethods, TestCase):
    date_class = Date
    sample_data = (
        (-214193, {'year': 161, 'month': 7, 'day': 15}),
        (-61387, {'year': 580, 'month': 3, 'day': 6}),
        (25469, {'year': 818, 'month': 2, 'day': 22}),
        (764652, {'year': 2843, 'month': 4, 'day': 20})
    )
