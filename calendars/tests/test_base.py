from typing import Dict
from unittest import TestCase

from calendars.base import BaseDate
from calendars.utils.base import Duration, Fixed


class RataDieDate(BaseDate):
    def to_parts(self) -> Dict[str, int]:  # type: ignore[override]
        return {'day': int(self)}

    @classmethod
    def from_parts(cls, day: int) -> 'RataDieDate':  # type: ignore[override]
        return cls(day)


class WeekDate(BaseDate):
    def to_parts(self) -> Dict[str, int]:  # type: ignore[override]
        week = self // 7
        day = self % 7
        return {'week': week, 'day': day}

    @classmethod
    # type: ignore[override]
    def from_parts(cls, week: int, day: int) -> 'WeekDate':
        return cls(week * 7 + day)


class TestBaseDate(TestCase):
    def test_construction(self) -> None:
        date: BaseDate = RataDieDate(1)
        self.assertEqual(Fixed(1), date.fixed)
        self.assertEqual({'day': 1}, date.to_parts())

        date = WeekDate(week=2, day=1)
        self.assertEqual(Fixed(15), date.fixed)
        self.assertEqual({'week': 2, 'day': 1}, date.to_parts())

    def test_conversion(self) -> None:
        date1 = RataDieDate(1)
        date2 = WeekDate(date1)
        self.assertEqual(date1.fixed, date2.fixed)
        self.assertEqual({'week': 0, 'day': 1}, date2.to_parts())

        date1 = RataDieDate(-14)
        date2 = WeekDate(date1)
        self.assertEqual(date1.fixed, date2.fixed)
        self.assertEqual({'week': -2, 'day': 0}, date2.to_parts())

    def test_addition(self) -> None:
        date1: BaseDate = WeekDate(week=1, day=0)
        self.assertEqual(
            WeekDate(week=2, day=0).fixed,
            date1 + Duration(days=7),
        )

        date1 = RataDieDate(2)
        date2 = RataDieDate(5)
        with self.assertRaises(TypeError):
            date2 + date1

        with self.assertRaises(ValueError):
            date1 + Duration(days='.01')

        self.assertEqual(2, date1 + Duration(days=0))
        self.assertEqual(3, date1 + Duration(days=1))

    def test_subtraction(self) -> None:
        date1 = RataDieDate(2)
        date2 = RataDieDate(5)
        self.assertEqual(Duration(days=3), date2 - date1)

        date3 = WeekDate(week=1, day=0)
        self.assertEqual(Duration(days=2), date3 - date2)
