from decimal import Decimal
from unittest import TestCase

from calendars.julian_days import EPOCH, EPOCH_DAY
from calendars.utils.base import Fixed, Moment


class TestEpoch(TestCase):
    def test_rata_die(self) -> None:
        self.assertEqual(EPOCH_DAY, Fixed(-1721425))
        self.assertEqual(EPOCH, Moment(-1721425 + Decimal('.5')))
