from unittest import TestCase

from calendars.julian_days import Date

from ..test_date import TestDateMethods


class TestDate(TestDateMethods, TestCase):
    date_class = Date
    sample_data = (
        (-214193, {'day': 1507231}),
        (-61387, {'day': 1660037}),
        (25469, {'day': 1746893}),
        (764652, {'day': 2486076}),
    )
