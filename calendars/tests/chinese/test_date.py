from unittest import TestCase

from calendars import chinese

from ..test_date import TestDateMethods


class TestChineseDate(TestDateMethods, TestCase):
    date_class = chinese.Date
    sample_data = (
        (-214193, {'year': -586, 'month': 7, 'day': 24}),
        (-61387, {'year': -168, 'month': 12, 'day': 5}),
        (25469, {'year': 70, 'month': 9, 'day': 24}),
    )
