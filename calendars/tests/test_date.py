from calendars.utils.base import Fixed


# Put test methods in a mixin so they don't get picked up by discovery.
class TestDateMethods:
    def test_from_parts(self) -> None:
        for expected_fixed, parts in self.sample_data:
            with self.subTest(**parts):
                self.assertEqual(
                    expected_fixed,
                    Fixed(self.date_class.from_parts(**parts))
                )

    def test_to_parts(self) -> None:
        for fixed, expected_parts in self.sample_data:
            with self.subTest(fixed=fixed):
                self.assertEqual(
                    expected_parts,
                    self.date_class(fixed).to_parts(),
                )

    def test_fixed_roundtrip(self) -> None:
        for fixed, _ in self.sample_data:
            date = self.date_class(fixed)
            self.assertEqual(
                fixed,
                Fixed(self.date_class.from_parts(**date.to_parts())),
            )

    def test_parts_roundtrip(self) -> None:
        for _, parts in self.sample_data:
            self.assertEqual(
                parts,
                self.date_class(**parts).to_parts(),
            )
