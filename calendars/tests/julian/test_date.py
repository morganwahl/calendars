from unittest import TestCase

from calendars.julian import Date

from ..test_date import TestDateMethods


class TestDate(TestDateMethods, TestCase):
    date_class = Date
    sample_data = (
        (-214193, {'year': -587, 'month': 7, 'day': 30}),
        (-61387, {'year': -169, 'month': 12, 'day': 8}),
        (25469, {'year': 70, 'month': 9, 'day': 26}),
        (764652, {'year': 2094, 'month': 7, 'day': 5})
    )
