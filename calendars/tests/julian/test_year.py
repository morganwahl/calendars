from unittest import TestCase

from calendars.julian import Year


class TestNew(TestCase):
    def test_basic(self) -> None:
        self.assertEqual(Year(1), 1)
        self.assertEqual(Year(-1), -1)

    def test_zero(self) -> None:
        with self.assertRaises(ValueError):
            Year(0)

    def test_bce(self) -> None:
        self.assertEqual(Year(1, bce=False), 1)
        self.assertEqual(Year(1, bce=True), -1)
        self.assertEqual(Year(-1, bce=True), -1)
        with self.assertRaises(ValueError):
            Year(-1, bce=False)

    def test_ce(self) -> None:
        self.assertEqual(Year(1, ce=False), -1)
        self.assertEqual(Year(1, ce=True), 1)
        self.assertEqual(Year(-1, ce=False), -1)
        with self.assertRaises(ValueError):
            Year(-1, ce=True)


class TestIsLeap(TestCase):
    def test_is_leap(self) -> None:
        self.assertFalse(Year(1).is_leap)
        self.assertTrue(Year(4).is_leap)
        self.assertTrue(Year(-1).is_leap)
        self.assertFalse(Year(-4).is_leap)
