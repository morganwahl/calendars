import math
from enum import IntEnum
from typing import Dict, Union

from . import julian_days
from .base import BaseDate, DayStart
from .utils.base import Duration
from .utils.math import remainder

# egyptian-epoch in D&R
EPOCH = julian_days.Date(day=1448638)


class Month(IntEnum):
    THOTH = 1
    PHAOPHI = 2
    ATHYR = 3
    CHOIAK = 4
    TYBI = 5
    MECHIR = 6
    PHAMENOTH = 7
    PHARMUTHI = 8
    PACHON = 9
    PAYNI = 10
    EPIPHI = 11
    MESORI = 12
    EPAGOMENAE = 13

    @property
    def named(self) -> bool:
        return self != self.EPAGOMENAE

    def length(self) -> Duration:
        return Duration(days=30) if self.named else Duration(days=5)


class Date(BaseDate):
    day_start = DayStart.DAWN

    @classmethod
    def from_parts(cls, year: int, month: Month, day: int) -> 'Date':  # type: ignore[override]  # noqa: E501
        """
        fixed-from-egyptian in D&R
        """
        return cls(EPOCH + 365 * (year - 1) + 30 * (month - 1) + day - 1)

    def to_parts(self) -> Dict[str, Union[int, Month]]:  # type: ignore[override]  # noqa: E501
        """
        egyptian-from-fixed in D&R
        """
        days = self - EPOCH
        year = math.floor(days / 365) + 1
        month = Month(math.floor(remainder(days, 365) / 30) + 1)
        day = int(days - 365 * (year - 1) - 30 * (month - 1) + 1)
        return dict(year=year, month=month, day=day)
