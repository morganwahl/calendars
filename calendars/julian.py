import math
from decimal import Decimal
from enum import IntEnum
from typing import Dict, Optional, Union

from . import gregorian
from .base import BaseDate
from .utils.base import Fixed
from .utils.math import remainder

# julian-epoch in D&R
EPOCH = Fixed(gregorian.Date(year=0, month=gregorian.Month.DECEMBER, day=30))


class Year(int):
    def __new__(
        cls,
        year: int,
        ce: Optional[bool]=None,
        bce: Optional[bool]=None,
    ) -> 'Year':
        if year == 0:
            raise ValueError("Julian years don't use 0.")
        if ce is None:
            if bce is not None:
                ce = not bce
            else:
                ce = year > 0
        if bce is None:
            if ce is not None:
                bce = not ce
        if ce == bce:
            raise ValueError("ce and bce are both {!r}".format(bool(ce)))
        if bce and year > 0:
            year = - year
        if not bce and year < 0:
            raise ValueError(
                "What does a negative year with bce=True (or ce=False) mean?")
        new_obj: Year = super().__new__(cls, year)  # type: ignore[no-any-return, call-arg]  # noqa: E501
        return new_obj

    @property
    def is_leap(self) -> bool:
        """
        julian-leap-year? in D&R
        """
        return remainder(self, 4) == (0 if self > 0 else 3)

    @property
    def bce(self) -> bool:
        return self < 0


class Month(IntEnum):
    JANUARY = 1
    FEBRUARY = 2
    MARCH = 3
    APRIL = 4
    MAY = 5
    JUNE = 6
    JULY = 7
    AUGUST = 8
    SEPTEMBER = 9
    OCTOBER = 10
    NOVEMBER = 11
    DECEMBER = 12


class DayOfMonth(int):
    pass


class Date(BaseDate):

    @classmethod
    def from_parts(cls, year: Year, month: Month, day: int) -> 'Date':  # type: ignore[override]  # noqa: E501
        """
        fixed-from-julian in D&R
        """
        year = Year(year)
        y = year + 1 if year < 0 else year
        return cls(sum((
            EPOCH - 1,
            365 * (y - 1),
            math.floor((y - 1) / Decimal(4)),
            math.floor((1 / Decimal(12)) * (367 * month - 362)),
            (0 if month <= 2 else (-1 if year.is_leap else -2)),
            day,
        )))

    def to_parts(self) -> Dict[str, Union[Year, Month, int]]:  # type: ignore[override]  # noqa: E501
        """
        julian-from-fixed in D&R
        """
        approx = math.floor((1 / Decimal(1461)) * (4 * (self - EPOCH) + 1464))
        year = Year(approx - 1 if approx <= 0 else approx)
        prior_days = self - Date(year=year, month=Month.JANUARY, day=1)
        if self < Date(year=year, month=Month.MARCH, day=1):
            correction = 0
        elif year.is_leap:
            correction = 1
        else:
            correction = 2
        month = Month(math.floor(
            (1 / Decimal(367))
            * (12 * (prior_days + correction) + 373)
        ))
        day = int(self - Date(year=year, month=month, day=1) + 1)
        return dict(year=year, month=month, day=day)
