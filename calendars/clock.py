import math
from decimal import Decimal
from typing import Dict

from .base import BaseTime, BaseTimeDelta
from .utils.math import remainder

HOURS_PER_DAY = Decimal(24)
MINUTES_PER_HOUR = Decimal(60)
MINUTES_PER_DAY = HOURS_PER_DAY * MINUTES_PER_HOUR
SECONDS_PER_MINUTE = Decimal(60)
SECONDS_PER_DAY = MINUTES_PER_DAY * SECONDS_PER_MINUTE


class Time(BaseTime):

    @classmethod
    # type: ignore[override]
    def from_parts(
        cls,
        # TODO make types for the parts
        hour: Decimal=Decimal(0),
        minute: Decimal=Decimal(0),
        second: Decimal=Decimal(0),
    ) -> 'Time':
        return cls(Decimal(sum((
            Decimal(hour) / HOURS_PER_DAY,
            Decimal(minute) / MINUTES_PER_DAY,
            Decimal(second) / SECONDS_PER_DAY,
        ))))

    def to_parts(self) -> Dict[str, Decimal]:  # type: ignore[override]
        total_hours = self * HOURS_PER_DAY
        total_minutes = total_hours * MINUTES_PER_HOUR
        total_seconds = total_minutes * SECONDS_PER_MINUTE
        return dict(
            hour=Decimal(math.floor(total_hours)),
            minute=Decimal(
                math.floor(remainder(total_minutes, MINUTES_PER_HOUR))
            ),
            second=Decimal(math.floor(
                remainder(
                    total_seconds,
                    SECONDS_PER_MINUTE * MINUTES_PER_HOUR
                )
            )),
        )


class TimeDelta(BaseTimeDelta):

    @classmethod
    # type: ignore[override]
    def from_parts(
        cls,
        # TODO make types for the parts
        hours: Decimal=Decimal(0),
        minutes: Decimal=Decimal(0),
        seconds: Decimal=Decimal(0),
    ) -> 'TimeDelta':
        return cls(Decimal(sum((
            Decimal(hours) / HOURS_PER_DAY,
            Decimal(minutes) / MINUTES_PER_DAY,
            Decimal(seconds) / SECONDS_PER_DAY,
        ))))

    def to_parts(self) -> Dict[str, Decimal]:  # type: ignore[override]
        total_hours = self * HOURS_PER_DAY
        total_minutes = total_hours * MINUTES_PER_HOUR
        total_seconds = total_minutes * SECONDS_PER_MINUTE
        return dict(
            hours=Decimal(math.floor(total_hours)),
            minutes=Decimal(
                math.floor(remainder(total_minutes, MINUTES_PER_HOUR))
            ),
            seconds=Decimal(math.floor(
                remainder(
                    total_seconds,
                    SECONDS_PER_MINUTE * MINUTES_PER_HOUR
                )
            )),
        )
