import math
from decimal import Decimal
from typing import Dict

from . import julian
from .base import BaseDate, BaseDateTime, BaseTime, DayStart
from .utils.base import Duration, Moment, Offset

EPOCH_DAY = julian.Date(
    year=julian.Year(4713, bce=True), month=julian.Month.JANUARY, day=1)
EPOCH_TIME = Offset('.5')

# jd-epoch in D&R
EPOCH = Moment(EPOCH_DAY) + Duration(days=EPOCH_TIME)


class Date(BaseDate):

    day_start = DayStart.NOON

    @classmethod
    def from_parts(cls, day: Decimal) -> 'Date':  # type: ignore[override]
        """
        fixed-from-jd in D&R
        """
        return cls(math.floor(EPOCH + Duration(days=day)))

    def to_parts(self) -> Dict[str, Decimal]:  # type: ignore[override]
        """
        jd-from-fixed in D&R
        """
        return dict(day=self - EPOCH)


class Time(BaseTime):

    @classmethod
    def from_parts(cls, offset: Decimal) -> 'Time':  # type: ignore[override]
        raise NotImplementedError

    def to_parts(self) -> Dict[str, Decimal]:  # type: ignore[override]
        raise NotImplementedError


class DateTime(BaseDateTime):

    @classmethod
    def from_parts(cls, day: Decimal) -> 'DateTime':
        """
        moment-from-jd in D&R
        """
        return cls(moment=Moment(day + EPOCH))

    def to_parts(self) -> Dict[str, Decimal]:
        """
        jd-from-moment in D&R
        """
        return {'day': self - EPOCH}
