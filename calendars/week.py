from enum import IntEnum

from .utils.base import Duration, Fixed
from .utils.math import remainder

# Fixed day 1 is a monday.
EPOCH = Fixed(0)


class WeekDay(IntEnum):
    SUNDAY = 0
    MONDAY = 1
    TUESDAY = 2
    WEDNESDAY = 3
    THURSDAY = 4
    FRIDAY = 5
    SATURDAY = 6

    @classmethod
    def date(cls, date: Fixed) -> 'WeekDay':
        """
        day-of-week-from-fixed in D&R
        """
        return cls(remainder(date - EPOCH, 7))

    @classmethod
    def weekday_on_or_before(cls, k: 'WeekDay', date: Fixed) -> Fixed:
        """
        kday-on-or-before in D&R
        """
        return date - cls.date(date - k)

    @classmethod
    def weekday_on_or_after(cls, k: 'WeekDay', date: Fixed) -> Fixed:
        """
        kday-on-or-after in D&R
        """
        return cls.weekday_on_or_before(k, date + Duration(days=6))

    @classmethod
    def weekday_nearest(cls, k: 'WeekDay', date: Fixed) -> Fixed:
        """
        kday-nearest in D&R
        """
        return cls.weekday_on_or_before(k, date + Duration(days=3))

    @classmethod
    def weekday_before(cls, k: 'WeekDay', date: Fixed) -> Fixed:
        """
        kday-before in D&R
        """
        return cls.weekday_on_or_before(k, date - Duration(days=1))

    @classmethod
    def weekday_after(cls, k: 'WeekDay', date: Fixed) -> Fixed:
        """
        kday-after in D&R
        """
        return cls.weekday_on_or_before(k, date + 7)
